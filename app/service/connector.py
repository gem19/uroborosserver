import mysql.connector
from mysql.connector import Error
import configparser
import logging


class DB_connector(object):
    # Creating a ConfigParser object to read the configuration file
    config = configparser.ConfigParser()
    """The DB_connector class is used to connect to the database.

    Attributes are
     * config: Сonfiguration file;
     * host_name: MySQL host;
     * user_name: Database Username (login);
     * user_password: Password of the database user;
     * db_name: Database name.

    Methods:
        __init__():
            Class constructor;
        create_connection():
            Creates a database connection.
    """

    def __init__(self):
        """Method (class constructor) for initializing attributes.

        Behaviour:
            Reads the config and initializes with the received data:
         host_name, user_name, user_password, db_name.
        """

        self.config.read("config/config.ini")
        self.host_name = self.config["BD"]["host_name"]
        self.user_name = self.config["BD"]["user_name"]
        self.user_password = self.config["BD"]["user_password"]
        self.db_name = self.config["BD"]["db_name"]

    def create_connection(self):
        """Method for connecting to the database.

        Behaviour:
            Using the connect() function, establishes a connection
         to the database.

        Exceptions:
            In case of exceptions, throws an Error exception.

        Returns:
            connection: MySqlConnection object.
        """

        connection = None
        logging.basicConfig(filename="DataBase.log", level=logging.INFO)
        log = logging.getLogger("ex")
        # Connect to MySQL database
        try:
            connection = mysql.connector.connect(
                host=self.host_name,
                user=self.user_name,
                passwd=self.user_password,
                database=self.db_name
            )
            print("Connection to MySQL DB successful")
        except Error as e:
            print(f"The error '{e}' occurred")
            log.error(f"{e}")
        return connection


if __name__ == '__main__':
    # Creating a class object
    db = DB_connector()
    # Connecting to the database"""
    connect = db.create_connection()
    # Closing the connection to the database
    connect.close()