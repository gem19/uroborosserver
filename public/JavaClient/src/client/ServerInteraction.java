package client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Files;

public class ServerInteraction {

 
     
// Метод: На основе данных, сгенерированных пользователем, введите адрес для
// подключения к серверу
//Если на сервер отправляется запрос GET и
// если пользователя интересует список групп определенного профессора,
// он собирает адрес с get_group.В противном случае он проверяет третий
// элемент массива на наличие дня недели.Если
// элемент пуст, он объединяет адрес с get_all.В противном
// случае он объединяет адрес с get_day.Если
// на сервер отправляется запрос POST, адрес собирается с помощью метода add
     
     
     
    // addressData - Массив с данными для адресной сборки.
    //  listMode - Список групп определенного профессора (1-Да, 2-Нет).
    //  method - метод запроса (1-GET,2-POST) .
    // возвращает адрес подключения
     
     //сборка адресса по (Массиву с данными для адресной сборки/Список групп определенного профессора/
     // метод запроса )
    public static String addressAssembly(String[] addressData, int listMode, int method) {
        String address = "";
        //если запрос на получение информации 
        if(method==1)
           
            if (listMode != 1) {
                if (addressData[2] == null) {
                  // собирает адресс со всем если не указан день
                    address = "/get_all/?" + addressData[0] + "=" + addressData[1];
                } else {
                    // собирает адресс с датой
                    address = "/get_day/?" + addressData[0] + "=" + addressData[1] + "&" + addressData[0] + "=" + addressData[2];
                }
            } else {
                // сборка адресса с группой
                address =  "/get_group/?" + addressData[0] + "=" + addressData[1];
            }
        else{
        // POST, адрес собирается с помощью метода add
          address="/add;" + addressData[0];  
        }

        return address;
    }

  
 
    // метод конекта к серверу по адркссу  через HttpURLConnection.
    
    //address - адрес подключения.
     //возвращает HttpURLConnection обьект

    
    public static HttpURLConnection сreateСonnection(String address) throws IOException {
        HttpURLConnection result = null;
        try {
            /* Попытка потключения по адрессу */
            URL url = new URL("http://localhost:8080" + address);
            System.out.printf("Trying to open a connection to the address %s...\n", url);
            final HttpURLConnection con = (HttpURLConnection) url.openConnection();
            System.out.println("Connection opened successfully...  ");

            result = con;
        } catch (IOException ex) {
            ex.printStackTrace();
            result = null;
        } finally {
            return result;
        }
    }

    
     // Метод: Создание запрос GET на сервер.
     
    //  con - HttpURLConnection обьект.
    // возвращает HttpURLConnection обьект.

   
    public static HttpURLConnection createGetRequest(HttpURLConnection con) throws IOException {
        HttpURLConnection result = null;
        try {
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            result = con;
        } catch (IOException ex) {
            ex.printStackTrace();
            result = null;
        } finally {
            return result;
        }
    }
    
 
     //Метод: Создает и отправляет POST-запрос на сервер
     
     // con - HttpURLConnection обьект.
     // filePath - путь кфайлу
 
  
    public static void sendPOSTRequest(HttpURLConnection con, String filePath) throws IOException {
        File file =new File(filePath);
        String fileName=file.getName();
        try {
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            con.setRequestProperty("Accept", "application/json");
            System.out.println("\nSending 'POST' request to URL : " + con.getURL() );
            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
            //wr.writeBytes("Content-Disposition: form-data;filename=\"" +fileName);
            byte[] bytes = Files.readAllBytes(file.toPath());
            wr.write(bytes);
            wr.flush();
        }
            
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
           System.out.printf("Server respons:%s\n", con.getResponseCode());
           con.disconnect();
        }
    }


  //  Метод: Отправляет запрос Get на сервер и считывает содержимое, полученное в ответ.
     
     // con - HttpURLConnection обьек.
     // возвращает содержимое, полученное с сервера.

    
    public static String sendGetRequest(HttpURLConnection con) throws IOException {
        String result = null;
        
        System.out.println("\nSending 'GET' request to URL : " + con.getURL() );
        System.out.printf("Server respons:%s\n", con.getResponseCode());
        con.setConnectTimeout(10000);
        con.setReadTimeout(10000);

        final StringBuilder content;
        try (final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            content = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();

            result = content.toString();
        } catch (final Exception ex) {
            ex.printStackTrace();
            result = "";
        } finally {
            con.disconnect();
            return result;
        }

    }

}
