package client;

/**
 * Класс для форматирования. Метод для создания строк для вывода,
 * полученный json разделяется для форматированного вывода. Параметром
 * метода является содержимое файла, возвращаемый параметр - разобранный
 * json. Форматирование данных происходит путем замены подстроки, которая
 * соответствует заданному регулярному выражению.
 * Данный код легок для понимания и выполняет заданные функции, комментарии 
 * присутствуют и верно описывают работу метода. Цикл для форматирования
 * имеет корректные условия завершения.
 * 
 * @author Valeria
 */
public class TableFormatter {

    /**
     * Method: The resulting json is split for formatted output.
     *
     * @param content - json content
     * @return table - parsed json
     */
    public String[] createPrintableStringFromJson(String content) {
            String[] table;
            /* Splitting the json */
            table = content.split("], \\[");
            /* Formatting data for output */
            for (int i = 0; i < table.length; i++) {
                table[i] = table[i].replaceAll("[\\{\\[\\]\\}\"]", "");
                table[i]=table[i].replaceAll("[a-zA-Z_]{1,}: ", "");
                table[i] = table[i].replaceAll(", ", "_");
            }
            return table;
    }
}
