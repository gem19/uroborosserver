package client;

import java.util.Scanner;

/**
 * Класс для ведения диалога с пользователем. Он позволяет узнать по каким
 * парметрам нужно получить расписание, происходит выбор типа запроса, выбор
 * преподавателя по которому будет отображаться список групп, дня недели,
 * аудитории и возможность выхода из программы. Данный код легок для понимания,
 * названия переменных и функций понятны. Присутствует проверка ввода данных
 * с выводом сообщения для пользователя. комментарии 
 * присутствуют и верно описывают работу каждого метода.
 * 
 * @author Valeria
 */
public class Console {

    /**
     * список полей.
     */
    static int list = 0;
    /**
     * Формат вывода таблицы расписания.
     */
    private static final String tableFormat = "%-20s|%-20s|%-20s|%-20s|%-60s|%-20s|%-12s|%-60s|%-8s|%-8s\n";
    /**
     * Формат вывода групп конкретного преподавателя.
     */
    private static final String professorsFormat = "%-12s|%-60s\n";

    /**
     * Method: получает значение поля списка.
     *
     * @return значение поля списка.
     */
    public static int getList() {
        return list;
    }

    /**
     * В формате диалога с пользователем он узнает по каким параметрам 
     * ему нужно получить расписание. Для начала пользователь выбирает
     * тип запроса, который будет отправлен на сервер, если выбран параметр
     * Преподаватель, то запрашивает пользователю отображать список групп 
     * определенного преподавателя или нет. Если ответ «Нет», спрашивает,
     * интересует ли пользователя расписание на определенный день недели. 
     * Если выбран параметр «Выход», программа выключается.
     *
     * @return массив с данными, выбранными пользователем.
     */
    public static int[] chooseMode() {

        Scanner in = new Scanner(System.in);
        int method;
        //выбранный параметр
        int param;
        //день недели
        int day = 0;
        System.out.print("Which method do you want to work with?:\n"
                            + "1.GET\n"
                            + "2.POST\n"
                            + "Choose one:");
        method=in.nextInt();
        /* Вывод меню выбора параметров*/
        System.out.print(
                "What table do you want to work with?:\n"
                + "1. Group;\n"
                + "2. Professor;\n"
                + "3. Auditorium;\n"
                + "4. Exit");

        do {
            System.out.print("\nChoose one:");
            param = in.nextInt();
            if (param < 1 || param > 4) {
                System.out.println("Please, choose correct value");
            }
        } while (param < 1 || param > 4);

        /* если выбран выход */
        if (param == 4) {
            System.exit(0);
        }

        /* пункт с преподавателями */
        if (param == 2 && method==1) {
            System.out.print(
                    "Are you interested in list of groups in which a certain professor leads?\n"
                    + "1. Yes;\n"
                    + "2. No.");
            do {
                System.out.print("\nChoose one:");
                list = in.nextInt();
                if (list < 0 || list > 2) {
                    System.out.println("Please, choose correct value");
                }
            } while (list < 1 || list > 2);
        }

        /* Если пользователя НЕ интересует список групп определенного преподавателя */
        if (list != 1 && method==1) {
            System.out.print(
                    "Are you interested in the schedule for a particular day?\n"
                    + "1. Yes;\n"
                    + "2. No.");
            do {
                System.out.print("\nChoose one:");
                day = in.nextInt();
                if (day < 0 || day > 2) {
                    System.out.println("Please, choose correct value");
                }
            } while (day < 1 || day > 2);
        }

        return new int[]{param, day, method};
    }

    /**
     * Method: В формате диалога, в зависимости от ранее выбранного 
     * пользователем параметра, формирует данные для адресной сборки.
     * В зависимости от того, какой запрос отправляется на сервер,
     * если пользователя интересует определенный день недели и не интересует
     * в списке групп определенного профессора запрашивает у пользователя 
     * название дня недели.
     *
     * @param param - Параметр, выбранный пользователем.
     * @param day - День недели
     * @param method - метод запроса
     * @return массив с данными 
     */
    public static String[] inputAddressData(int param, int day, int method) {
        Scanner in = new Scanner(System.in);
        String[] addressData = new String[]{null, null, null};
        //Если метод запроса GET
        if(method==1)
        switch (param) {
            /* Если выбран параметр Группа */
            case 1: {
                addressData[0] = "Group";
                System.out.print("Enter the name of the group whose schedule you are looking for:");
                addressData[1] = in.nextLine();
                break;
            }

            /* Если выбран параметр Преподаватель */
            case 2: {
                addressData[0] = "Professor";
                System.out.print("Enter the name of the professor whose schedule you are looking for:");
                addressData[1] = in.nextLine();
                break;
            }

            /* Если выбран параметр аудитория */
            case 3: {
                addressData[0] = "Auditorium";
                System.out.print("Enter the name of the auditorium whose schedule you are looking for(Example - D-508):");
                addressData[1] = in.nextLine();
                break;
            }
        }
        else //Если метод запроса POST
           switch (param){
           case 1: {
                addressData[0] = "Group";
                break;
            }

            /* Если выбран параметр Преподаватель */
            case 2: {
                addressData[0] = "Professor";
                break;
            }

            /* Если выбран параметр аудитория */
            case 3: {
                addressData[0] = "Auditorium";
                System.out.print("Enter the path to the .json file from which you want to add data to the table 'Auditorium':");
                addressData[1]=in.nextLine();
                break;
            } 
        }
        /* если нужен конкретный день */
        if (list != 1 && day != 2 && method==1) {
            System.out.print("\nEnter day names:");
            addressData[2] = in.nextLine();
        }

        return addressData;
    }

    /**
     * Method: Разбирает и форматирует, выводит расписание на консоль
     * в зависимости от того, интересует пользователя список групп 
     * определенного профессора или нет.
     *
     * @param schedule - таблица с расписанием для вывода.
     */
    public static void outputData(String[] schedule) {
        if (!"".equals(schedule[0])) {
            /* Вывод данных в зависимости от режима списка */
            if (list != 1) {
                System.out.printf(tableFormat, "Day of the week", "Lesson start time", "Lesson end time", "Parity of week", "Subject name", "Lesson type", "Group name", "Professor", "Housing", "Cabinet");
                for (String s : schedule) {
                    String[] temp = s.split("_");
                    System.out.printf(tableFormat, temp[0], temp[1], temp[2], temp[3], temp[4], temp[5], temp[6], temp[7], temp[8], temp[9]);
                }
                System.out.println();
            } else {
                System.out.printf(professorsFormat, "Group", "Professor");
                for (String s : schedule) {
                    String[] temp = s.split("_");
                    System.out.printf(professorsFormat, temp[0], temp[1]);
                }
                System.out.println();
            }
        } else {
            System.out.println("The schedule table is empty!");
        }
    }

}
