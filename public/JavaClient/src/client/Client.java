package client;

import java.io.IOException;
import java.net.HttpURLConnection;

public class Client {

    public static void main(String[] args) throws IOException {
    // запуск клиента 1 точка входа  
        startClient();
    }

  // метод старта клиента
    public static void startClient() throws IOException {
    //создание консоль
        Console console = new Console();
        //Получить параметры расписания от пользователя
        int[] chooseData = console.chooseMode();
        int list = console.getList();
        // создает взаимодействие с сервером
        ServerInteraction serverInteraction = new ServerInteraction();
        //Генерирует данные для адреса и собирает их масив строк
        String[] addressData = console.inputAddressData(chooseData[0], chooseData[1], chooseData[2]);
        String address = serverInteraction.addressAssembly(addressData, list, chooseData[2]);
        //сборка адреса 
        String content = null;
        try {
            // пытаемся Подключение к серверу и отправка запроса на получение
            HttpURLConnection connect = serverInteraction.сreateСonnection(address);
            if(chooseData[2]==1){
            // конект = взаимодействие с сервером.получение запроса
                connect = serverInteraction.createGetRequest(connect);
                // контект = отправление запроса на сервер     
                content = serverInteraction.sendGetRequest(connect);
                //Сборка таблици с информацией из джейсон файла
                String[] table = new TableFormatter().createPrintableStringFromJson(content);
               //вывод таблици с информацией в консоль
                console.outputData(table); 
            }
            else{
            // отправка POST-запрос на сервер
                serverInteraction.sendPOSTRequest(connect, addressData[1]);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
